//
//  ClientesViewController.swift
//  MiPrimeraTabla
//
//  Created by Kevin Belter on 12/20/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import UIKit

struct CellConfigure {
    var item: Any
    var cellIdentifier: String
}

class ClientesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var configFiles: [CellConfigure]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let service = ClienteService()
        let clients = service.getAllClients()
        
        configFiles = clients.map { CellConfigure(item: $0, cellIdentifier: ClientTableViewCell.cellIdentifier) }
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension ClientesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let configFile = configFiles[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: configFile.cellIdentifier, for: indexPath) as! GenericCellProtocol
        cell.configure(item: configFile.item)
        return cell as! UITableViewCell
    }
}

extension ClientesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
