//
//  GenericCellProtocol.swift
//  MiPrimeraTabla
//
//  Created by Kevin Belter on 12/20/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import Foundation

protocol GenericCellProtocol {
    func configure(item: Any)
}
