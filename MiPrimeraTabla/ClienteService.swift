//
//  ClienteService.swift
//  MiPrimeraTabla
//
//  Created by Kevin Belter on 12/20/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import Foundation

class Cliente {
    var nombre: String
    var nombreFotoPerfil: String
    var cantidadCompras: Int
    
    init(nombre: String, nombreFotoPerfil: String, cantidadCompras: Int) {
        self.nombre = nombre
        self.nombreFotoPerfil = nombreFotoPerfil
        self.cantidadCompras = cantidadCompras
    }
}

class ClienteService {
    func getAllClientsNames() -> [String] {
        return [
            "Pepe",
            "Dario",
            "Jaime",
            "Wilson",
            "Martin",
            "Pocho",
            "Copito",
            "Delarua",
            "Pepe",
            "Dario",
            "Jaime",
            "Wilson",
            "Martin",
            "Pocho",
            "Copito",
            "Delarua"
        ]
    }
    
    func getAllClients() -> [Cliente] {
        return [
            Cliente(nombre: "Pepe", nombreFotoPerfil: "1", cantidadCompras: 10),
            Cliente(nombre: "Dario", nombreFotoPerfil: "2", cantidadCompras: 50),
            Cliente(nombre: "Jaime", nombreFotoPerfil: "3", cantidadCompras: 60),
            Cliente(nombre: "Wilson", nombreFotoPerfil: "4", cantidadCompras: 44),
            Cliente(nombre: "Jordan", nombreFotoPerfil: "5", cantidadCompras: 30),
            Cliente(nombre: "Patricio", nombreFotoPerfil: "6", cantidadCompras: 12),
            Cliente(nombre: "Pocho", nombreFotoPerfil: "7", cantidadCompras: 10),
            Cliente(nombre: "Nico", nombreFotoPerfil: "8", cantidadCompras: 11),
        ]
    }
}
