//
//  ClientesTableViewController.swift
//  MiPrimeraTabla
//
//  Created by Kevin Belter on 12/20/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import UIKit

class ClientesTableViewController: UITableViewController {

    private let cellIdentifier = "ClientCellId"
    
    private var clients: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let clientService = ClienteService()
        
        self.clients = clientService.getAllClientsNames()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        super.numberOfSections(in: tableView)
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        super.tableView(tableView, numberOfRowsInSection: section)
        return clients.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        super.tableView(tableView, cellForRowAt: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = clients[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //super.tableView(tableView, didSelectRowAt: indexPath) //No se proque no hay que llamar al super.
        let client = clients[indexPath.row]
    }
}
