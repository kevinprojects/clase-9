//
//  ClientTableViewCell.swift
//  MiPrimeraTabla
//
//  Created by Kevin Belter on 12/20/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import UIKit

class ClientTableViewCell: UITableViewCell, GenericCellProtocol {

    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblCantidadCompras: UILabel!
    
    static var cellIdentifier: String { return "ClientCellId" }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.cornerRadius = 25.0
    }
    
    func configure(item: Any) {
        guard let cliente = item as? Cliente else { return }
        lblNombre.text = cliente.nombre.capitalized
        lblCantidadCompras.text = "\(cliente.cantidadCompras)"
        imgProfile.image = UIImage(named: cliente.nombreFotoPerfil)
    }
}
